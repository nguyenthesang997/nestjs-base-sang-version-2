## Getting started

```bash
cp .env.example .env
# 3. Install dependencies. (Make sure yarn is installed: https://yarnpkg.com/lang/en/docs/install)
yarn
### Development
```bash
# 4. Run development server and open http://localhost:3000
yarn start:dev
# 5. Read the documentation linked below for "Setup and development".
```

### Build
To build the App, run

```bash
yarn build:prod
```

And you will see the generated file in `dist` that ready to be served.
