import { Column, Entity, OneToOne } from 'typeorm';

import type { IAbstractEntity } from '../../common/abstract.entity';
import { AbstractEntity } from '../../common/abstract.entity';
import { RoleType } from '../../constants';
import { UseDto, VirtualColumn } from '../../decorators';
import { BasicInformationEntity } from '../onboarding/entities/basic-information.entity';
import { PageInformationEntity } from '../onboarding/entities/page-information.entity';
import { SportProfilesEntity } from '../onboarding/entities/sport-profiles.entity';
import type { UserDtoOptions } from './dtos/user.dto';
import { UserDto } from './dtos/user.dto';
export interface IUserEntity extends IAbstractEntity<UserDto> {
  firstName?: string;

  lastName?: string;

  role: RoleType;

  email?: string;

  password?: string;

  phone?: string;

  avatar?: string;

  fullName?: string;
}

@Entity({ name: 'users' })
@UseDto(UserDto)
export class UserEntity
  extends AbstractEntity<UserDto, UserDtoOptions>
  implements IUserEntity
{
  @Column({ nullable: true })
  firstName?: string;

  @Column({ nullable: true })
  lastName?: string;

  @Column({ type: 'enum', enum: RoleType, default: RoleType.USER })
  role: RoleType;

  @Column({ unique: true, nullable: true })
  email?: string;

  @Column({ nullable: true })
  password?: string;

  @Column({ nullable: true })
  phone?: string;

  @Column({ nullable: true })
  avatar?: string;

  @VirtualColumn()
  fullName?: string;

  @OneToOne(() => BasicInformationEntity, (basicInfo) => basicInfo.user, {
    nullable: true,
  })
  basicInformation: BasicInformationEntity;

  @OneToOne(() => PageInformationEntity, (pageInfo) => pageInfo.user, {
    nullable: true,
  })
  pageInformation: PageInformationEntity;

  @OneToOne(() => SportProfilesEntity, (sportProfile) => sportProfile.user, {
    nullable: true,
  })
  sportProfile: SportProfilesEntity;
}
