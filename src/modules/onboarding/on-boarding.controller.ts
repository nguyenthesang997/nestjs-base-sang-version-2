import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { RoleType } from '../../constants';
import { Auth, AuthUser } from '../../decorators';
import { UserEntity } from '../user/user.entity';
import { BasicInformationDto } from './dto/basic-information.dto';
import { CreateBasicInformationDto } from './dto/create-basic-information.dto';
import { CreatePageInformationDto } from './dto/create-page-information.dto';
import { CreateSportProfileDto } from './dto/create-sport-profile.dto';
import { PageInformationDto } from './dto/page-information.dto';
import { SportProfilesDto } from './dto/sport-profiles.dto';
import { BasicInformationService } from './services/basic-information.service';
import { PageInformationService } from './services/page-information.service';
import { SportProfileService } from './services/sport-profile.service';

@Controller('on-boarding')
@ApiTags('On Boarding')
export class OnBoardingController {
  constructor(
    private readonly basicInformationService: BasicInformationService,
    private readonly pageInformationService: PageInformationService,
    private readonly sportProfile: SportProfileService,
  ) {}

  @Post('basic-information')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: BasicInformationDto,
    description: 'Create basic information success',
  })
  async createBasicInformation(
    @Body() data: CreateBasicInformationDto,
    @AuthUser() user: UserEntity,
  ) {
    return this.basicInformationService.createBasicInformation(data, user);
  }

  @Post('page-information')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: PageInformationDto,
    description: 'Create page information success',
  })
  async createPageInformation(
    @Body() data: CreatePageInformationDto,
    @AuthUser() user: UserEntity,
  ) {
    return this.pageInformationService.createPageInformation(data, user);
  }

  @Get('basic-information')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: BasicInformationDto,
    description: 'get basic information of user',
  })
  async getBasicInformation(@AuthUser() user: UserEntity) {
    return this.basicInformationService.getBasicInformation(user);
  }

  @Get('page-information')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: PageInformationDto,
    description: 'get page information of user',
  })
  async getPageInformation(@AuthUser() user: UserEntity) {
    return this.pageInformationService.getPageInformation(user);
  }

  @Get('sport-profile')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: SportProfilesDto,
    description: 'get sport profile of user',
  })
  async getSportProfile(@AuthUser() user: UserEntity) {
    return this.sportProfile.getSportProfile(user);
  }

  @Post('sport-profile')
  @Auth([RoleType.USER])
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: SportProfilesDto,
    description: 'create sport profile of user',
  })
  async createSportProfile(
    @AuthUser() user: UserEntity,
    @Body() data: CreateSportProfileDto,
  ) {
    return this.sportProfile.createSportProfile(user, data);
  }
}
