import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BasicInformationEntity } from './entities/basic-information.entity';
import { PageInformationEntity } from './entities/page-information.entity';
import { SportProfilesEntity } from './entities/sport-profiles.entity';
import { SportsEntity } from './entities/sports.entity';
import { OnBoardingController } from './on-boarding.controller';
import { BasicInformationService } from './services/basic-information.service';
import { PageInformationService } from './services/page-information.service';
import { SportProfileService } from './services/sport-profile.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BasicInformationEntity,
      PageInformationEntity,
      SportsEntity,
      SportProfilesEntity,
    ]),
  ],
  controllers: [OnBoardingController],
  providers: [
    SportProfileService,
    BasicInformationService,
    PageInformationService,
  ],
})
export class OnBoardingModule {}
