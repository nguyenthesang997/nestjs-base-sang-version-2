import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../../../common/abstract.entity';
import { UserEntity } from '../../user/user.entity';
import type { SportProfilesDto } from '../dto/sport-profiles.dto';
import { SportsEntity } from './sports.entity';

@Entity({ name: 'sport_profiles' })
export class SportProfilesEntity extends AbstractEntity<SportProfilesDto> {
  @Column({
    name: 'current_team',
    nullable: true,
  })
  currentTeam: string;

  @Column({
    name: 'goal',
  })
  goal: string;

  @OneToOne(() => UserEntity, (user) => user.sportProfile)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;

  @ManyToMany(() => SportsEntity, (sport) => sport.users)
  @JoinTable({
    name: 'sport_profiles_items',
    joinColumn: { name: 'sport_profile_id' },
    inverseJoinColumn: { name: 'sport_id' },
  })
  sports: SportsEntity[];
}
