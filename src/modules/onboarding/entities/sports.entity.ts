import { Column, Entity, ManyToMany } from 'typeorm';

import { AbstractEntity } from '../../../common/abstract.entity';
import { SportProfilesEntity } from './sport-profiles.entity';

@Entity({
  name: 'sports',
})
export class SportsEntity extends AbstractEntity {
  @Column()
  name: string;

  @ManyToMany(
    () => SportProfilesEntity,
    (sportProfile) => sportProfile.sports,
    {
      onDelete: 'CASCADE',
    },
  )
  users: SportProfilesEntity[];
}
