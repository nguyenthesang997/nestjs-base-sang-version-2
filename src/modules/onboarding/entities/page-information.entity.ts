import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../../../common/abstract.entity';
import { UseDto } from '../../../decorators';
import { UserEntity } from '../../user/user.entity';
import { PageInformationDto } from '../dto/page-information.dto';

@Entity({ name: 'page_information' })
@UseDto(PageInformationDto)
export class PageInformationEntity extends AbstractEntity<PageInformationDto> {
  @Column({ name: 'tag_line', length: 100 })
  tagLine: string;

  @Column()
  tags: string;

  @OneToOne(() => UserEntity, (user) => user.pageInformation)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
