import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../../../common/abstract.entity';
import { Gender } from '../../../constants/gender';
import { UseDto } from '../../../decorators';
import { UserEntity } from '../../user/user.entity';
import { BasicInformationDto } from '../dto/basic-information.dto';
@Entity({ name: 'basic_information' })
@UseDto(BasicInformationDto)
export class BasicInformationEntity extends AbstractEntity<BasicInformationDto> {
  @Column({ name: 'date_of_birth' })
  dateOfBirth: Date;

  @Column({ type: 'enum', enum: Gender, default: Gender.MALE })
  gender: string;

  @Column({ name: 'nationality_id', nullable: true })
  nationalityId: number;

  @Column({ nullable: true, length: 500 })
  story: string;

  @OneToOne(() => UserEntity, (user) => user.basicInformation)
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
