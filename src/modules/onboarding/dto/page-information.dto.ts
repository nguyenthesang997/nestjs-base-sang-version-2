import { ApiProperty } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/abstract.dto';

export class PageInformationDto extends AbstractDto {
  @ApiProperty()
  tagLine: string;

  @ApiProperty()
  tags: string;
}
