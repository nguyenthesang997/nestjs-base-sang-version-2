import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';

export class CreatePageInformationDto {
  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(100)
  tagLine: string;

  @ApiProperty({ required: true })
  @IsString()
  tags: string;
}
