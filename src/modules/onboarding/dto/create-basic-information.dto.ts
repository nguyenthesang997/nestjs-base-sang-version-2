import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsString } from 'class-validator';

import { Gender } from '../../../constants/gender';

export class CreateBasicInformationDto {
  @ApiProperty({ required: true })
  @IsOptional()
  dateOfBirth: Date;

  @ApiProperty({ required: true, enum: Gender })
  @IsString()
  gender: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @Type(() => Number)
  nationalityId: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  story: string;
}
