import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/abstract.dto';
import { UserDto } from '../../user/dtos/user.dto';

export class SportProfilesDto extends AbstractDto {
  @ApiPropertyOptional()
  currentTeam: string;

  @ApiPropertyOptional()
  goal: string;

  @ApiPropertyOptional()
  user: UserDto;
}
