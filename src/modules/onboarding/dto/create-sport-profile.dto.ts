import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID } from 'class-validator';

export class CreateSportProfileDto {
  @ApiProperty({ required: true })
  @IsOptional()
  @IsUUID()
  sportId: string;

  @ApiProperty({ required: false })
  @IsString()
  currentTeam: string;

  @ApiProperty({ required: true })
  @IsOptional()
  @IsString()
  goal: string;
}
