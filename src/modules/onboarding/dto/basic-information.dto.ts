import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/abstract.dto';
import { Gender } from '../../../constants/gender';
import { UserDto } from '../../user/dtos/user.dto';

export class BasicInformationDto extends AbstractDto {
  @ApiProperty()
  dateOfBirth: Date;

  @ApiProperty({ enum: Gender })
  gender: string;

  @ApiPropertyOptional()
  nationalityId: number;

  @ApiPropertyOptional()
  story: string;

  @ApiPropertyOptional()
  user: UserDto;
}
