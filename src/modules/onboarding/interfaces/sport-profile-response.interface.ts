export interface ISportProfilesResponse<T> {
  data: T;
  success: boolean;
}
