import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import type { UserEntity } from '../../user/user.entity';
import type { CreateBasicInformationDto } from '../dto/create-basic-information.dto';
import { BasicInformationEntity } from '../entities/basic-information.entity';

@Injectable()
export class BasicInformationService {
  constructor(
    @InjectRepository(BasicInformationEntity)
    private basicInformationRepo: Repository<BasicInformationEntity>,
  ) {}

  async createBasicInformation(
    data: CreateBasicInformationDto,
    user: UserEntity,
  ) {
    const basicInformation = this.basicInformationRepo.create({
      ...data,
      user,
    });

    await this.basicInformationRepo.save(basicInformation);

    return basicInformation;
  }

  async getBasicInformation(user: UserEntity) {
    const result = await this.basicInformationRepo.findOneBy({
      user: {
        id: user.id,
      },
    });

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }
}
