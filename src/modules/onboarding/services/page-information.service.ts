import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import type { UserEntity } from '../../user/user.entity';
import type { CreatePageInformationDto } from '../dto/create-page-information.dto';
import { PageInformationEntity } from '../entities/page-information.entity';

@Injectable()
export class PageInformationService {
  constructor(
    @InjectRepository(PageInformationEntity)
    private pageInformationRepo: Repository<PageInformationEntity>,
  ) {}

  async createPageInformation(
    data: CreatePageInformationDto,
    user: UserEntity,
  ) {
    const pageInformation = this.pageInformationRepo.create({
      ...data,
      user,
    });
    await this.pageInformationRepo.save(pageInformation);

    return pageInformation;
  }

  async getPageInformation(user: UserEntity) {
    const result = await this.pageInformationRepo.findOneBy({
      user: {
        id: user.id,
      },
    });

    if (!result) {
      throw new NotFoundException();
    }

    return result;
  }
}
