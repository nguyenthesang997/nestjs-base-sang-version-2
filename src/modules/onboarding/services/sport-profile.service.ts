import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import type { UserEntity } from '../../user/user.entity';
import type { CreateSportProfileDto } from '../dto/create-sport-profile.dto';
import { SportProfilesEntity } from '../entities/sport-profiles.entity';
import { SportsEntity } from '../entities/sports.entity';
import type { ISportProfilesResponse } from '../interfaces/sport-profile-response.interface';
@Injectable()
export class SportProfileService {
  private logger = new Logger('SportProfileService');

  constructor(
    @InjectRepository(SportProfilesEntity)
    private sportProfileRepository: Repository<SportProfilesEntity>,
    @InjectRepository(SportsEntity)
    private sportRepository: Repository<SportsEntity>,
  ) {}

  async createSportProfile(user: UserEntity, data: CreateSportProfileDto) {
    // check userId exists
    try {
      const sport = await this.sportRepository.findOneBy({
        id: data.sportId as Uuid,
      });

      if (!sport) {
        throw new NotFoundException();
      }

      const sportProfile = this.sportProfileRepository.create({
        ...data,
        user,
      });
      await this.sportProfileRepository.save(sportProfile);

      return sportProfile;
    } catch (error) {
      if (error.code === '23505') {
        //Duplicate email
        throw new ConflictException(`Sport profile already exists.`);
      }

      throw new InternalServerErrorException(error.message);
    }
  }

  async getSportProfile(
    user: UserEntity,
  ): Promise<ISportProfilesResponse<SportProfilesEntity>> {
    const result = await this.sportProfileRepository.findOneBy({
      user: {
        id: user.id,
      },
    });

    if (!result) {
      throw new NotFoundException();
    }

    return {
      data: result,
      success: true,
    };
  }
}
