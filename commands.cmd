multiple line to single line by \n: "sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' {path-to-file}"

migrate database:
      yarn migration:generate --name={migration-name}
      yarn migration:create --name={migration-name}
      yarn migration:revert --name={migration-name}
      yarn migration:run
